package eu.gnomino.plotit;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;

public class PlotIt extends JavaPlugin implements Listener {
	private final HashMap<UUID, HashMap<String, Location>> selections = new HashMap<UUID, HashMap<String, Location>>();
	public static Economy econ = null;
	public static Permission perms = null;
	private ConfigurationSection messages = null;

	public void onEnable() {
		saveDefaultConfig();
		this.getServer().getPluginManager().registerEvents(this, this);
		if (!setupEconomy()) {
			getLogger()
					.severe("Disabled due to no Vault compatible economy plugin found!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		setupPermissions();
		messages = getConfig().getConfigurationSection("messages");
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer()
				.getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> rsp = getServer()
				.getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
		return perms != null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("gettool")) {
			Material m = Material.getMaterial(getConfig().getString("tool"));
			Player p = (Player) sender;
			ItemStack is = new ItemStack(m, 1);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(ChatColor.translateAlternateColorCodes('&',
					getConfig().getString("tool_name")));
			is.setItemMeta(im);
			p.getInventory().addItem(is);
			p.sendMessage(getMsg("tool_msg"));
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("claim")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (selections.containsKey(p.getUniqueId())) {
					HashMap<String, Location> psel = selections.get(p
							.getUniqueId());
					if (psel.size() == 2) {
						Location s1 = psel.get("left");
						Location s2 = psel.get("right");
						if (s1.getWorld().getName() != s2.getWorld().getName()) {
							p.sendMessage(getMsg("selection_required"));
							return true;
						}
						int blocks = 0;
						int y, x, z = 0;
						y = s1.getBlockY() - s2.getBlockY();
						if (y < 0) {
							y *= -1;
						}
						y++;
						x = s1.getBlockX() - s2.getBlockX();
						if (x < 0) {
							x *= -1;
						}
						x++;
						z = s1.getBlockZ() - s2.getBlockZ();
						if (z < 0) {
							z *= -1;
						}
						z++;
						blocks = x * y * z;
						double price = getConfig().getDouble("block_price")
								* blocks;
						int maxclaims = 0;
						int maxv = 0;
						int maxh = 0;
						ConfigurationSection maxhs = getConfig()
								.getConfigurationSection("max_xz");
						ConfigurationSection maxvs = getConfig()
								.getConfigurationSection("max_y");
						ConfigurationSection maxs = getConfig()
								.getConfigurationSection("max");
						for (String group : perms.getPlayerGroups(p)) {
							if (maxs.contains(group)) {
								int n = maxs.getInt(group);
								if ((n > maxclaims && maxclaims != -1)
										|| n == -1) {
									maxclaims = n;
								}
							}
							if (maxhs.contains(group)) {
								int o = maxhs.getInt(group);
								if ((o > maxh && maxh != -1) || o == -1) {
									maxh = o;
								}
							}
							if (maxvs.contains(group)) {
								int q = maxvs.getInt(group);
								if ((q > maxv && maxv != -1) || q == -1) {
									maxv = q;
								}
							}
						}
						if (maxv != -1 && y > maxv) {
							p.sendMessage(getMsg("too_high").replaceAll(
									"{MAX}", "" + maxv));
							return true;
						}
						if (maxh != -1 && (x > maxh || z > maxh)) {
							p.sendMessage(getMsg("too_large").replaceAll(
									"{MAX}", "" + maxh));
							return true;
						}
						ConfigurationSection nbs = getConfig()
								.getConfigurationSection("nb_claims");
						int nbclaims = 0;
						if (nbs.contains(p.getUniqueId().toString())) {
							nbclaims = nbs.getInt(p.getUniqueId().toString());
						}
						if (maxclaims == -1 || maxclaims > nbclaims) {
							EconomyResponse t = econ.withdrawPlayer(
									(OfflinePlayer) p, price);
							if (t.transactionSuccess()) {
								nbs.set(p.getUniqueId().toString(),
										nbclaims + 1);
								saveConfig();
								ProtectedCuboidRegion pr = new ProtectedCuboidRegion(
										p.getUniqueId() + "_" + nbclaims,
										toBlockV(s1), toBlockV(s2));
								DefaultDomain d = pr.getOwners();
								WorldGuardPlugin wg = (WorldGuardPlugin) getServer()
										.getPluginManager().getPlugin(
												"WorldGuard");
								d.addPlayer(wg.wrapPlayer(p));
								pr.setOwners(d);
								RegionManager rm = wg.getRegionManager(s1
										.getWorld());
								if (rm.getApplicableRegions(pr).size() != 0) {
									p.sendMessage(getMsg("already_someone"));
									econ.depositPlayer((OfflinePlayer) p, price); // Refund
																					// the
																					// player
									return true;
								}
								rm.addRegion(pr);
								try {
									rm.save();
								} catch (ProtectionDatabaseException e) {
									e.printStackTrace();
									econ.depositPlayer((OfflinePlayer) p, price); // Refund
																					// the
																					// player
									p.sendMessage(getMsg("error_saving"));
									return true;
								}
								p.sendMessage(getMsg("success_claim")
										.replaceAll("{PRICE}",
												econ.format(price)));
							} else {
								p.sendMessage(getMsg("transaction_failed"));
							}
							return true;
						} else {
							p.sendMessage(getMsg("too_many_claims"));
						}
					} else {
						p.sendMessage(getMsg("selection_required"));
						return true;
					}
				} else {
					p.sendMessage(getMsg("selection_required"));
					return true;
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("plotit") && args.length == 1) {
			if (args[0].equalsIgnoreCase("reload")) {
				if (sender.hasPermission("plotit.reload")) {
					reloadConfig();
					Command.broadcastCommandMessage(sender, getMsg("reloaded"));
					return true;
				} else {
					sender.sendMessage(getMsg("no_permission"));
					return true;
				}
			}
			if (args[0].equalsIgnoreCase("help")) {
				if (sender.hasPermission("plotit.help")) {
					Map<String, Map<String, Object>> cmds = this
							.getDescription().getCommands();
					for (String c : cmds.keySet()) {
						Map<String, Object> p = cmds.get(c);
						sender.sendMessage(ChatColor.GOLD + "/" + c + " "
								+ ChatColor.GREEN + p.get("description"));
					}
					return true;
				} else {
					sender.sendMessage(getMsg("no_permission"));
					return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		ItemStack is = e.getItem();
		if (is != null
				&& is.getType() == Material.getMaterial(getConfig().getString(
						"tool"))
				&& is.getItemMeta()
						.getDisplayName()
						.equals(ChatColor.translateAlternateColorCodes('&',
								getConfig().getString("tool_name")))) {
			e.setCancelled(true);
			HashMap<String, Location> psel = new HashMap<String, Location>();
			if (selections.containsKey(e.getPlayer().getUniqueId())) {
				psel = selections.get(e.getPlayer().getUniqueId());
			}
			if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
				psel.put("left", e.getClickedBlock().getLocation());
				e.getPlayer().sendMessage(getMsg("selection_left"));
				e.getPlayer().sendBlockChange(
						e.getClickedBlock().getLocation(), Material.GLOWSTONE,
						(byte) 0);
			}
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				psel.put("right", e.getClickedBlock().getLocation());
				e.getPlayer().sendMessage(getMsg("selection_right"));
				e.getPlayer().sendBlockChange(
						e.getClickedBlock().getLocation(), Material.GLOWSTONE,
						(byte) 0);
			}
			selections.put(e.getPlayer().getUniqueId(), psel);
		}

	}

	public com.sk89q.worldedit.BlockVector toBlockV(Location l) {
		return new com.sk89q.worldedit.BlockVector(l.getBlockX(),
				l.getBlockY(), l.getBlockZ());
	}

	public String getMsg(String msg) {
		return ChatColor.translateAlternateColorCodes('&',
				messages.getString(msg));
	}

	public void onDisable() {
		saveConfig();
	}
}
